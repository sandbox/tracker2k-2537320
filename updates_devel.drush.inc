<?php

/**
 * @file
 * Drush implementation.
 */

/**
 * Implements hook_drush_command().
 */
function updates_devel_drush_command() {
  $items['ud-list'] = array(
    'description' => 'List schema version updates of the specific module.',
    'aliases' => array('udl'),
    'arguments' => array(
      'module' => 'Module name.',
    ),
  );

  $items['ud-get'] = array(
    'description' => 'Get current schema version (latest performed update) of the specific module.',
    'aliases' => array('udg'),
    'arguments' => array(
      'module' => 'Module name.',
    ),
  );

  $items['ud-set'] = array(
    'description' => 'Get current schema version (latest performed update) for the specific module.',
    'aliases' => array('uds'),
    'arguments' => array(
      'module' => 'Module name.',
      'schema' => 'Schema version (latest performed update).',
    ),
  );

  $items['ud-perform'] = array(
    'description' => 'Perform specific schema version update hook of the specific module.',
    'aliases' => array('udp'),
    'arguments' => array(
      'module' => 'Module name.',
      'schema' => 'Schema version.',
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function updates_devel_drush_help($section) {
  list($drush, $command) = explode(':', $section);

  if ($drush == 'drush') {
    $commands = updates_devel_drush_command();
    if (isset($commands[$command])) {
      return dt($commands[$command]['description']);
    }
  }

  return NULL;
}

/**
 * Callback for the ud-list command.
 */
function drush_updates_devel_ud_list($module_name = NULL) {
  if (!isset($module_name)) {
    return drush_set_error('error', dt('Please specify module name.'));
  }

  if (!drush_module_exists($module_name)) {
    return drush_set_error('error', dt('Specified module doesn\'t exist or disabled.'));
  }

  if (module_load_install($module_name) === FALSE) {
    drush_print(dt('No update hooks found.'));
    return NULL;
  }

  $text = dt('List of updates for !module module:', array('!module' => $module_name));
  drush_print($text);
  drush_print();

  $columns = drush_get_context('DRUSH_COLUMNS', FALSE);
  $updates_found = FALSE;

  for ($i = 7000; $i <= 7999; $i++) {
    $update_hook = $module_name . '_update_' . $i;
    if (function_exists($update_hook)) {
      $updates_found = TRUE;
      drush_print('- ' . $i);

      $func = new ReflectionFunction($update_hook);
      $description_str = $func->getDocComment();
      $description_str = $description_str ? $description_str : '';
      $description_str = str_replace(array('*', '/'), ' ', $description_str);
      $description = explode("\n", $description_str);

      foreach ($description as $key => &$line) {
        if (!trim($line)) {
          continue;
        }

        $text = '';
        if ($columns === FALSE) {
          $text = $line;
        }
        else {
          $text = truncate_utf8($line, $columns - 3, TRUE, TRUE);
        }

        drush_print($text, 2);
      }

      drush_print();
    }
  }

  if (!$updates_found) {
    drush_print(dt('No update hooks found.'));
  }

  return NULL;
}

/**
 * Callback for the ud-get command.
 */
function drush_updates_devel_ud_get($module_name = NULL) {
  if (!isset($module_name)) {
    return drush_set_error('error', dt('Please specify module name.'));
  }

  if (!drush_module_exists($module_name)) {
    return drush_set_error('error', dt('Specified module doesn\'t exist or disabled.'));
  }

  // Make sure the installation API is available
  require_once DRUSH_DRUPAL_CORE . '/includes/install.inc';

  $schema = drupal_get_installed_schema_version($module_name);
  drush_print(dt('Schema version: !update', array('!update' => $schema)));

  return NULL;
}

/**
 * Callback for the ud-set command.
 */
function drush_updates_devel_ud_set($module_name = NULL, $schema_version = NULL) {
  if (!isset($module_name)) {
    return drush_set_error('error', dt('Please specify module name.'));
  }

  if (!drush_module_exists($module_name)) {
    return drush_set_error('error', dt('Specified module doesn\'t exist or disabled.'));
  }

  if ($schema_version === NULL) {
    return drush_set_error('error', dt('Please specify schema version.'));
  }

  // Make sure the installation API is available
  require_once DRUSH_DRUPAL_CORE . '/includes/install.inc';

  $schema = drupal_get_installed_schema_version($module_name);
  drush_print(dt('Old schema version: !schema', array('!schema' => $schema)));
  drupal_set_installed_schema_version($module_name, $schema_version);
  $schema = drupal_get_installed_schema_version($module_name);
  drush_print(dt('New schema version: !schema', array('!schema' => $schema)));

  return NULL;
}

/**
 * Callback for the ud-perform command.
 */
function drush_updates_devel_ud_perform($module_name = NULL, $schema_version = NULL) {
  if (!isset($module_name)) {
    return drush_set_error('error', dt('Please specify module name.'));
  }

  if (!drush_module_exists($module_name)) {
    return drush_set_error('error', dt('Specified module doesn\'t exist or disabled.'));
  }

  if ($schema_version === NULL) {
    return drush_set_error('error', dt('Please specify schema version.'));
  }

  $schema_version = intval($schema_version);
  if (!(($schema_version >= 7000) && ($schema_version <= 7999))) {
    return drush_set_error('error', dt('Schema version must be between 7000 - 7999.'));
  }

  if (module_load_install($module_name) === FALSE) {
    return drush_set_error('error', dt('No update hooks found.'));
  }

  $update_hook = $module_name . '_update_' . $schema_version;
  if (!function_exists($update_hook)) {
    return drush_set_error('error', dt('Update hook for !schema schema version is not found.', array('!schema' => $schema_version)));
  }

  _drush_updates_devel_update_batch($module_name, $update_hook);

  return NULL;
}

function _drush_updates_devel_update_batch($module, $function) {
  $operations = array();
  $operations[] = array('_drush_updates_devel_update_do_one', array($module, $function));

  $batch['operations'] = $operations;
  $batch += array(
    'title' => 'Updating',
    'init_message' => 'Starting updates',
    'error_message' => 'An unrecoverable error has occurred. You can find the error message below. It is advised to copy it to the clipboard for reference.',
  );
  batch_set($batch);
  drush_backend_batch_process();
}

function _drush_updates_devel_update_do_one($module, $function, &$context) {
  $context['log'] = FALSE;

  $ret = array();
  module_load_install($module);
  if (function_exists($function)) {
    try {
      drush_log("Executing " . $function);
      $ret['results']['query'] = $function($context['sandbox']);

      // If the update hook returned a status message (common in batch updates),
      // show it to the user.
      if ($ret['results']['query']) {
        drush_log($ret['results']['query'], 'ok');
      }

      $ret['results']['success'] = TRUE;
    }
    catch (Exception $e) {
      $ret['#abort'] = array('success' => FALSE, 'query' => $e->getMessage());
      drush_set_error('DRUPAL_EXCEPTION', $e->getMessage());
    }
  }

  if (isset($context['sandbox']['#finished'])) {
    $context['finished'] = $context['sandbox']['#finished'];
    unset($context['sandbox']['#finished']);
  }

  if (!empty($ret['#abort'])) {
    // Record this function in the list of updates that were aborted.
    $context['results']['#abort'][] = $function;
  }

  $context['message'] = 'Performed update: ' . $function;
}
